# translation of krita-org.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: krita-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-10-31 01:51+0000\n"
"PO-Revision-Date: 2023-03-12 12:25+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: config.yaml:0
msgid "Krita"
msgstr "Krita"

#: config.yaml:0
msgid ""
"Krita is a professional FREE and open source painting program. It is made by "
"artists that want to see affordable art tools for everyone."
msgstr ""

#: config.yaml:0
msgid "Learn"
msgstr "Učiť sa"

#: i18n/en.yaml:0
msgid "Recent Posts"
msgstr ""

#: i18n/en.yaml:0
msgid "Donate"
msgstr ""

#: i18n/en.yaml:0
msgid "Software"
msgstr ""

#: i18n/en.yaml:0
msgid "Education"
msgstr ""

#: i18n/en.yaml:0
msgid "Foundation"
msgstr ""

#: i18n/en.yaml:0
msgid "Language"
msgstr ""

#: i18n/en.yaml:0
msgid "Report a bug"
msgstr ""

#: i18n/en.yaml:0
msgid "Roadmap"
msgstr ""

#: i18n/en.yaml:0
msgid "Release History"
msgstr ""

#: i18n/en.yaml:0
msgid "Documentation"
msgstr ""

#: i18n/en.yaml:0
msgid "Source Code"
msgstr ""

#: i18n/en.yaml:0
msgid "FAQ"
msgstr ""

#: i18n/en.yaml:0
msgid "Privacy Statement"
msgstr ""

#: i18n/en.yaml:0
msgid "Tutorials"
msgstr ""

#: i18n/en.yaml:0
msgid "About"
msgstr ""

#: i18n/en.yaml:0
msgid "Donations"
msgstr ""

#: i18n/en.yaml:0
msgid "Get Involved"
msgstr ""

#: i18n/en.yaml:0
msgid "What is KDE"
msgstr ""

#: i18n/en.yaml:0
msgid "Website license"
msgstr ""

#: i18n/en.yaml:0
msgid "Contact"
msgstr ""

#: i18n/en.yaml:0
msgid "Sitemap"
msgstr ""

#: i18n/en.yaml:0
msgid "Previous Post"
msgstr ""

#: i18n/en.yaml:0
msgid "Next Post"
msgstr ""

#: i18n/en.yaml:0
msgid "{{ .ReadingTime }} minutes"
msgstr ""

#: i18n/en.yaml:0
msgid "Reading time:"
msgstr ""

#: i18n/en.yaml:0
msgid "Could you tell us something about yourself? "
msgstr ""

#: i18n/en.yaml:0
msgid "Download"
msgstr ""

#: i18n/en.yaml:0
msgid "Powered by"
msgstr ""

#: i18n/en.yaml:0
msgid "Art by "
msgstr ""

#: i18n/en.yaml:0
msgid "News"
msgstr ""

#: i18n/en.yaml:0
msgid "No News"
msgstr ""

#: i18n/en.yaml:0
msgid "See All"
msgstr ""

#: i18n/en.yaml:0
msgid "Tools You Need To Grow as an Artist"
msgstr ""

#: i18n/en.yaml:0
msgid "All the features you need"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Great for new and experienced artists. Here are some of the features that "
"Krita offers:"
msgstr ""

#: i18n/en.yaml:0
msgid "Multiple brush types for different art styles"
msgstr ""

#: i18n/en.yaml:0
msgid "Layers, drawing assistants, stabilizers"
msgstr ""

#: i18n/en.yaml:0
msgid "Animation tools to transform your artwork"
msgstr ""

#: i18n/en.yaml:0
msgid "Resources and Materials"
msgstr ""

#: i18n/en.yaml:0
msgid "Expand Krita’s capabilities with online tools and assets:"
msgstr ""

#: i18n/en.yaml:0
msgid "Brushes, patterns, and vector libraries"
msgstr ""

#: i18n/en.yaml:0
msgid "Texture packs and page templates"
msgstr ""

#: i18n/en.yaml:0
msgid "Plugins that add new functionality"
msgstr ""

#: i18n/en.yaml:0
msgid "See Resources"
msgstr ""

#: i18n/en.yaml:0
msgid "See features"
msgstr ""

#: i18n/en.yaml:0
msgid "FREE education and resources"
msgstr ""

#: i18n/en.yaml:0
msgid "See education"
msgstr ""

#: i18n/en.yaml:0
msgid "A supportive community"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"An online community for Krita artists to share artwork and tips with each "
"other."
msgstr ""

#: i18n/en.yaml:0
msgid "Share your artwork and get feedback to improve"
msgstr ""

#: i18n/en.yaml:0
msgid "Ask questions about using Krita or seeing if there is a bug"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Give feedback to developers when new features and plugins are being created"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit artist community"
msgstr ""

#: i18n/en.yaml:0
msgid "Artist Interviews"
msgstr ""

#: i18n/en.yaml:0
msgid "Free and Open Source"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is a public project licensed as GNU GPL, owned by its contributors. "
"That's why Krita is Free and Open Source software, forever."
msgstr ""

#: i18n/en.yaml:0
msgid "See details about license usage"
msgstr ""

#: i18n/en.yaml:0
msgid "Give back"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is made by people from all around the world – many of which are "
"volunteers. If you find Krita valuable and you want to see it improve, "
"consider becoming part of the development fund."
msgstr ""

#: i18n/en.yaml:0
msgid "Contribute to the development fund"
msgstr ""

#: i18n/en.yaml:0
msgid "Read more"
msgstr ""

#: i18n/en.yaml:0
msgid "Krita Sprint 2019 - Deventer, Netherlands"
msgstr ""

#: i18n/en.yaml:0
msgid "No trials."
msgstr ""

#: i18n/en.yaml:0
msgid "No subscriptions."
msgstr ""

#: i18n/en.yaml:0
msgid "No limit to your creativity."
msgstr ""

#: i18n/en.yaml:0
msgid "Clean and Flexible Interface"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"An intuitive user interface that stays out of your way. The dockers and "
"panels can be moved and customized for your specific workflow. Once you have "
"your setup, you can save it as your own workspace. You can also create your "
"own shortcuts for commonly used tools."
msgstr ""

#: i18n/en.yaml:0
msgid "Customizable Layout"
msgstr ""

#: i18n/en.yaml:0
msgid "Over 30 dockers for additional functionality"
msgstr ""

#: i18n/en.yaml:0
msgid "Dark and light color themes"
msgstr ""

#: i18n/en.yaml:0
msgid "Learn the interface"
msgstr ""

#: i18n/en.yaml:0
msgid "All the tools you need"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Over 100 professionally made brushes that come preloaded. These brushes give "
"a good range of effects so you can see the variety of brushes that Krita has "
"to offer."
msgstr ""

#: i18n/en.yaml:0
msgid "Beautiful Brushes"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Learn"
msgid "Learn more"
msgstr "Učiť sa"

#: i18n/en.yaml:0
msgid "Brush Stabilizers"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Have a shaky hand? Add a stabilizer to your brush to smoothen it out. Krita "
"includes 3 different ways to smooth and stabilize your brush strokes. There "
"is even a dedicated Dynamic Brush tool where you can add drag and mass."
msgstr ""

#: i18n/en.yaml:0
msgid "Vector & Text"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Built-in vector tools help you create comic panels. Select a word bubble "
"template from the vector library and drag it on your canvas. Change the "
"anchor points to create your own shapes and libraries. Add text to your "
"artwork as well with the text tool. Krita uses SVG to manage its vector "
"format."
msgstr ""

#: i18n/en.yaml:0
msgid "Brush Engines"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Customize your brushes with over 9 unique brush engines. Each engine has a "
"large amount of settings to customize your brush. Each brush engine is made "
"to satisfy a specific need such as the Color Smudge engine, Shape engine, "
"Particle engine, and even a filter engine. Once you are done creating your "
"brushes, you can save them and organize them with Krita's unique tagging "
"system."
msgstr ""

#: i18n/en.yaml:0
msgid "Wrap-around mode"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"It is easy to create seamless textures and patterns now. The image will make "
"references of itself along the x and y axis. Continue painting and watch all "
"of the references update instantly. No more clunky offsetting to see how "
"your image repeats itself."
msgstr ""

#: i18n/en.yaml:0
msgid "Resource Manager"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Import brush and texture packs from other artists to expand your tool set. "
"If you create some brushes that you love, share them with the world by "
"creating your own bundles. Check out the brush packs that are available in "
"the Resources area."
msgstr ""

#: i18n/en.yaml:0
msgid "Visit Resources Area"
msgstr ""

#: i18n/en.yaml:0
msgid "Simple and Powerful 2D Animation"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Turn Krita into an animation studio by switching to the animation workspace. "
"Bring your drawings to life by layering your animations, importing audio, "
"and fine tuning your frames. When you are finished, share with your friends "
"by exporting your creation to a video. Or just export the images to continue "
"working in another application."
msgstr ""

#: i18n/en.yaml:0
msgid "Features"
msgstr ""

#: i18n/en.yaml:0
msgid "Multiple layers and audio support"
msgstr ""

#: i18n/en.yaml:0
msgid "Supports 1,000s of frames on timeline"
msgstr ""

#: i18n/en.yaml:0
msgid "Playback controls with pausing, playing, and timeline scrubbing"
msgstr ""

#: i18n/en.yaml:0
msgid "Onion skinning support to help with in-betweens"
msgstr ""

#: i18n/en.yaml:0
msgid "Tweening with opacity and position changes"
msgstr ""

#: i18n/en.yaml:0
msgid "Change start time, end time, and FPS"
msgstr ""

#: i18n/en.yaml:0
msgid "Export results to video or still images"
msgstr ""

#: i18n/en.yaml:0
msgid "Drag and drop frames to organize timings"
msgstr ""

#: i18n/en.yaml:0
msgid "Shortcuts for duplicating and pulling frames"
msgstr ""

#: i18n/en.yaml:0
msgid "Performance tweaking with drop-frame option"
msgstr ""

#: i18n/en.yaml:0
msgid "Productivity features"
msgstr ""

#: i18n/en.yaml:0
msgid "Drawing Assistants"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Use a drawing aid to assist you with vanishing points and straight lines. "
"The Assistant Tool comes with unique assistants to help you make that "
"perfect shape. These tools range from drawing ellipses to creating "
"curvilinear perspective with the Fisheye Point tool."
msgstr ""

#: i18n/en.yaml:0
msgid "Layer Management"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"In addition to painting, Krita comes with vector, filter, group, and file "
"layers. Combine, order, and flatten layers to help your artwork stay "
"organized."
msgstr ""

#: i18n/en.yaml:0
msgid "Select & Transform"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Highlight a portion of your drawing to work on. There are additional "
"features that allow you to add and remove from the selection. You can "
"further modify your selection by feathering and inverting it. Paint a "
"selection with the Global Selection Mask."
msgstr ""

#: i18n/en.yaml:0
msgid "Full Color Management"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita supports full color management through LCMS for ICC and OpenColor IO "
"for EXR. This allows you to incorporate Krita into your existing color "
"management pipeline. Krita comes with a wide variety of ICC working space "
"profiles for every need."
msgstr ""

#: i18n/en.yaml:0
msgid "GPU Enhanced"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"With OpenGL or Direct3D enabled, you will see increased canvas rotation and "
"zooming speed. The canvas will also look better when zoomed out. The Windows "
"version supports Direct3D 11 in place of OpenGL."
msgstr ""

#: i18n/en.yaml:0
msgid "PSD Support"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Open PSD files that even Photoshop cannot open. Load and save to PSD when "
"you need to take your artwork across different programs."
msgstr ""

#: i18n/en.yaml:0
msgid "HDR Painting"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is the only painting application that lets you open, save, edit and "
"author HDR and scene-referred images. With OCIO and OpenEXR support, you can "
"manipulate the view to examine HDR images."
msgstr ""

#: i18n/en.yaml:0
msgid "Python Scripting"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Powerful API for creating your own widgets and extending Krita. With using "
"PyQt and Krita's own API, there are many possibilities. A number of plugins "
"come pre-installed for your reference."
msgstr ""

#: i18n/en.yaml:0
msgid "Training Resources"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"In addition to training and educational material found on the Internet, "
"Krita produces its own training material to help you learn all of the tools "
"fast."
msgstr ""

#: i18n/en.yaml:0
msgid "All of this (and more) for FREE"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is, and will always be, free software. There is a lot more to learn "
"than this overview page, but you should be getting a good idea of what Krita "
"can do."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Krita"
msgstr ""

#: i18n/en.yaml:0
msgid "Released on: "
msgstr ""

#: i18n/en.yaml:0
msgid "Release Notes"
msgstr ""

#: i18n/en.yaml:0
msgid "Windows Installer"
msgstr ""

#: i18n/en.yaml:0
msgid "Windows Portable"
msgstr ""

#: i18n/en.yaml:0
msgid "macOS Installer"
msgstr ""

#: i18n/en.yaml:0
msgid "Linux 64-bit AppImage"
msgstr ""

#: i18n/en.yaml:0
msgid "Flatpak"
msgstr ""

#: i18n/en.yaml:0
msgid "snap"
msgstr ""

#: i18n/en.yaml:0
msgid "Hosted on flathub."
msgstr ""

#: i18n/en.yaml:0
msgid "Maintained by the community."
msgstr ""

#: i18n/en.yaml:0
msgid "Run"
msgstr ""

#: i18n/en.yaml:0
msgid "Apple iPads and iPhones are not supported"
msgstr ""

#: i18n/en.yaml:0
msgid "Google Play Store"
msgstr ""

#: i18n/en.yaml:0
msgid "See individual Android APK builds"
msgstr ""

#: i18n/en.yaml:0
msgid "System Requirements"
msgstr ""

#: i18n/en.yaml:0
msgid " : "
msgstr ""

#: i18n/en.yaml:0
msgid "Operating System"
msgstr ""

#: i18n/en.yaml:0
msgid "Windows 8.1 or higher / macOS 10.14 / Linux"
msgstr ""

#: i18n/en.yaml:0
msgid "RAM"
msgstr ""

#: i18n/en.yaml:0
msgid "4 GB at minimum required. 16 GB recommended."
msgstr ""

#: i18n/en.yaml:0
msgid "GPU"
msgstr ""

#: i18n/en.yaml:0
msgid "OpenGL 3.0 or higher / Direct3D 11"
msgstr ""

#: i18n/en.yaml:0
msgid "Graphics Tablet Brand"
msgstr ""

#: i18n/en.yaml:0
msgid "Any tablet compatible with your operating system"
msgstr ""

#: i18n/en.yaml:0
msgid "Store version"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Paid versions of Krita on other platforms. You will get automatic updates "
"when new versions of Krita come out. After deduction of the Store fee, the "
"money will support Krita development. For the Microsoft store version you "
"will need Windows 10 or above."
msgstr ""

#: i18n/en.yaml:0
msgid "Microsoft Store"
msgstr ""

#: i18n/en.yaml:0
msgid "Steam Store"
msgstr ""

#: i18n/en.yaml:0
msgid "Epic Games Store"
msgstr ""

#: i18n/en.yaml:0
msgid "Mac App Store"
msgstr ""

#: i18n/en.yaml:0
msgid "Krita Next Nightly Builds"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"New builds created daily that have bleeding-edge features. This is for "
"testing purposes only. Use at your own risk."
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Krita"
msgid "Visit Krita Next"
msgstr "Krita"

#: i18n/en.yaml:0
msgid "Krita Plus Nightly Builds"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Contains only bug fixes on top of the currently released version of Krita"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Krita"
msgid "Visit Krita Plus"
msgstr "Krita"

#: i18n/en.yaml:0
msgid "Windows Shell Extension"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"The Shell extension is included with the Windows Installer. An optional add-"
"on for Windows that allow KRA thumbnails to appear in your file browser."
msgstr ""

#: i18n/en.yaml:0
msgid "Windows"
msgstr ""

#: i18n/en.yaml:0
msgid "macOS"
msgstr ""

#: i18n/en.yaml:0
msgid "Linux"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source application. You are free to study, modify, "
"and distribute Krita under GNU GPL v3 license."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Older Versions"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"If the newest version is giving you issues there are older versions "
"available for download. New versions of Krita on Windows do not support 32-"
"bit."
msgstr ""

#: i18n/en.yaml:0
msgid "GPG Signatures"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Used to verify the integrity of your downloads. If you don't know what GPG "
"is you can ignore it."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Signature"
msgstr ""

#: i18n/en.yaml:0
msgid "tarball"
msgstr ""

#: i18n/en.yaml:0
msgid "Git (KDE Invent)"
msgstr ""

#: i18n/en.yaml:0
msgid "Old Version Library"
msgstr ""

#: i18n/en.yaml:0
msgid "Last Windows 32-bit version"
msgstr ""

#: i18n/en.yaml:0
msgid "Hosted on snapcraft."
msgstr ""

#: i18n/en.yaml:0
msgid "Monthly Donations"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"If you join the Krita Development Fund, you will directly help keep Krita "
"getting better and better, and you will get the following:"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"A great application with powerful features, features designed together with "
"the Krita community"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Stable software where we will always try to fix issues—last year over 1,200 "
"issues were resolved!"
msgstr ""

#: i18n/en.yaml:0
msgid "Visibility and recognition online, via community badges"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit Development Fund"
msgstr ""

#: i18n/en.yaml:0
msgid "One-time Donation"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"One-time donations will take you to the Mollie payment portal. Donations are "
"done in euros as that provides the most payment options."
msgstr ""

#: i18n/en.yaml:0
msgid "Accepted payment methods:"
msgstr ""

#: i18n/en.yaml:0
msgid "Transaction ID: "
msgstr ""

#: i18n/en.yaml:0
msgid "Paid €%s"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"If there are issues with your donation, you can share this transaction ID "
"with us to help us know exactly which donation was yours."
msgstr ""

#: i18n/en.yaml:0
msgid "Back to website"
msgstr ""

#: i18n/en.yaml:0
msgid "Have Fun Painting"
msgstr ""

#: i18n/en.yaml:0
msgid "Having trouble downloading?"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit the Downloads area"
msgstr ""

#: i18n/en.yaml:0
msgid "What's Next"
msgstr ""

#: i18n/en.yaml:0
msgid "Connect with the Community"
msgstr ""

#: i18n/en.yaml:0
msgid "Share artwork and ask questions on the forum."
msgstr ""

#: i18n/en.yaml:0
msgid "Krita Artists forum"
msgstr ""

#: i18n/en.yaml:0
msgid "Watch videos to learn about new brushes and techniques."
msgstr ""

#: i18n/en.yaml:0
msgid "Krita YouTube channel"
msgstr ""

#: i18n/en.yaml:0
msgid "Free Learning Resources"
msgstr ""

#: i18n/en.yaml:0
msgid "Getting Started"
msgstr ""

#: i18n/en.yaml:0
msgid "User Interface"
msgstr ""

#: i18n/en.yaml:0
msgid "Basic Concepts"
msgstr ""

#: i18n/en.yaml:0
msgid "Animations"
msgstr ""

#: i18n/en.yaml:0
msgid "User Manual"
msgstr ""

#: i18n/en.yaml:0
msgid "Layers and Masks"
msgstr ""

#: i18n/en.yaml:0
msgid "Like what we are doing? Help support us"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source project. Please consider supporting the "
"project with donations or by buying training videos or the artbook! With "
"your support, we can keep the core team working on Krita full-time."
msgstr ""

#: i18n/en.yaml:0
msgid "Buy something"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"There should have been a video here but your browser does not seem to "
"support it."
msgstr ""

#: i18n/en.yaml:0
msgid "Nope. Not in here either... (404)"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Either the page does not exist any more, or the web address was typed wrong."
msgstr ""

#: i18n/en.yaml:0
msgid "Go back to the home page"
msgstr ""
