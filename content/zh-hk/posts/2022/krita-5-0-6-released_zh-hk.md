---
title: "發佈 Krita 5.0.6 修正版本"
date: "2022-04-27"
categories: 
  - "news_zh-hk"
  - "officialrelease_zh-hk"
---

今日我哋發佈 Krita 5.0.6。依個修正版本修正咗兩項會引起 crash 嘅問題：

- 喺使用向量圖層或者向量選取區域時，因為使用復原操作而引致 crash：[BUG:447985](https://bugs.kde.org/show_bug.cgi?id=447985)
- 刪除帶有動態透明度遮罩嘅向量圖層時會 crash：[BUG:452396](https://bugs.kde.org/show_bug.cgi?id=452396)

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 係自由、免費同開源嘅專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或者[購買教學影片](https://krita.org/en/shop/)支持我哋啦！有你哋嘅熱心支持，我哋先可以俾核心開發者全職為 Krita 工作。

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會同安裝版本共用設定檔同埋資源。如果想用免安裝版測試並回報 crash 嘅問題，請同時下載偵錯符號 (debug symbols)。

注意：我哋依家唔再提供為 32 位元 Windows 建置嘅版本。

- 64 位元安裝程式：[krita-x64-5.0.6-setup.exe](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-setup.exe)
- 64 位元免安裝版：[krita-x64-5.0.6.zip](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6.zip)
- [偵錯符號 Debug symbols（請解壓到 Krita 程式資料夾入面）](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-dbg.zip)

### Linux

- 64 位元 Linux AppImage：[krita-5.0.6-x86\_64.appimage](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6-x86_64.appimage)

Linux 版本依家唔使再另外下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你用緊 macOS Sierra 或者 High Sierra，請睇下[依段影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解點樣執行開發者簽署嘅程式。

- macOS 套件：[krita-5.0.6.dmg](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.dmg)

### Android

我哋提供嘅 ChomeOS 同 Android 版本仲係**測試版本**。依個版本或可能含有大量嘅 bug，而且仲有部份功能未能正常運作。由於使用者介面仲未改進好，軟件或者須要配合實體鍵盤先可以用到全部功能。Krita 唔啱俾 Android 智能電話用，只係啱平板電腦用，因為使用者介面嘅設計並未為細小嘅螢幕做最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86_64-5.0.6-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86-5.0.6-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-arm64-v8a-5.0.6-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-armeabi-v7a-5.0.6-release-signed.apk)

### 原始碼

- [krita-5.0.6.tar.gz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.gz)
- [krita-5.0.6.tar.xz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.0.6/](https://download.kde.org/stable/krita/5.0.6) 並撳落每個下載檔案嘅「Details」連結以查閱嗰個檔案嘅 MD5 / SHA1 / SHA256 校對碼。

### 數碼簽署

Linux AppImage 以及原始碼嘅 .tar.gz 同 .tar.xz 壓縮檔已使用數碼簽署簽名。你可以由[依度](https://files.kde.org/krita/4DA79EDA231C852B)取得 public key。簽名檔可以喺[依度](https://download.kde.org/stable/krita/5.0.6/)揾到（副檔名為 .sig）。
