---
title: "Thank you"
date: "2018-06-02"
layout: otdty
scssFiles:
- /scss/otdty.scss
jsFiles:
- /js/otdty.js
---

# Thank you for your donation

Everything helps when donating to the Krita foundation. We cherish this money and it goes directly to the developers who fix bugs and create new features.
