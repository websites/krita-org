---
title: "New Video: Discover Vector Shape Libraries"
date: "2022-05-31"
---

After a LOT of research, Ramon's new video is done: this time he investigates how to create vector libraries in Inkscape for use in Krita. And there are two cool libraries he has prepared for you all to play with!

{{< youtube spy-0EKBcvQ >}}
 