---
title: "Krita Monthly Update - Edition 22"
date: "2025-01-16"
---
Welcome to the [@Krita-promo](https://krita-artists.org/groups/krita-promo) team's December 2024 development and community update.

## Development Report
### Krita 5.2.9 Coming Soon
After taking some time off for the holidays, the team is getting ready to start the year with a new bugfix release. This will contain all the stable fixes from the community bug hunt and more, so stay tuned!

## Community Report
### December 2024 Monthly Art Challenge Results
For the ["Tropical"](https://krita-artists.org/t/monthly-art-challenge-december-2024-topic-tropical/108359/) theme, 16 members submitted 23 original artworks.
And the winner is… 
[Tropical vibes by @steve.improvthis](https://krita-artists.org/t/monthly-art-challenge-winner-december-2024-tropical-vibes/110875), with three entries.
<figure>
<a href="images/posts/2025/mu22_spotted_eagle_rays-steve_improvthis.jpeg"> <img class="fit" src="images/posts/2025/mu22_spotted_eagle_rays-steve_improvthis.jpeg" alt="Spotted Eagle Rays by @steve.improvthis"/> </a>
</figure>
Be sure to check out the other two artworks in the link as well!

### The January Art Challenge is Open Now
For the January Art Challenge, @steve.improvthis has chosen ["Magical Adventure"](https://krita-artists.org/t/monthly-art-challenge-january-2025-magical-adventure/111018) as the theme, with the optional challenge of choosing a point of visual focus. See the full brief for more details, and embark on a fantastical new journey.

## Featured Artwork
### Best of Krita-Artists - November/December 2024
Six images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-november-december-2024-nomination-submissions-thread/107020), which was open from November 14th to December 11th. When the poll closed on December 14th, these five wonderful works made their way onto the Krita-Artists featured artwork banner:

[Kurzschwardzenbuglen Nature Sanctuary by @Yaroslavus_Artem](https://krita-artists.org/t/109201)
<figure>
<a href="images/posts/2025/mu22_kurzschwardzenbuglen_nature_sanctuary-yaroslavus_artem.jpeg"> <img class="fit" src="images/posts/2025/mu22_kurzschwardzenbuglen_nature_sanctuary-yaroslavus_artem.jpeg" alt="Kurzschwardzenbuglen Nature Sanctuary by @Yaroslavus_Artem"/> </a>
</figure>

[Speedpainting 01122024 by @SylviaRitter](https://krita-artists.org/t/108543)
<figure>
<a href="images/posts/2025/mu22_speedpainting_01122024-sylviaritter.jpeg"> <img class="fit" src="images/posts/2025/mu22_speedpainting_01122024-sylviaritter.jpeg" alt="Speedpainting 01122024 by @SylviaRitter"/> </a>
</figure>

[Magicians Room by @Ape](https://krita-artists.org/t/107597)
<figure>
<a href="images/posts/2025/mu22_magicians_room-ape.jpeg"> <img class="fit" src="images/posts/2025/mu22_magicians_room-ape.jpeg" alt="Magicians Room by @Ape"/> </a>
</figure>

[Princess Mipha by @ynr_nohara and @zeki](https://krita-artists.org/t/106900)
<figure>
<a href="images/posts/2025/mu22_princess_mipha-ynr_nohara-zeki.jpeg"> <img class="fit" src="images/posts/2025/mu22_princess_mipha-ynr_nohara-zeki.jpeg" alt="Princess Mipha by @ynr_nohara and @zeki"/> </a>
</figure>

[[otsoa] Finished illustrations by @onde_hurlante](https://krita-artists.org/t/24748)
<figure>
<a href="images/posts/2025/mu22_corbeau-onde_hurlante.jpeg"> <img class="fit" src="images/posts/2025/mu22_corbeau-onde_hurlante.jpeg" alt="Corbeau by @onde_hurlante"/> </a>
</figure>




## Ways to Help Krita
Krita is Free and Open Source Software developed by an international team of sponsored developers and volunteer contributors.

Visit [Krita's funding page](https://krita.org/en/donations/) to see how user donations keep development going, and explore a one-time or monthly contribution. Or check out more ways to [Get Involved](https://krita.org/en/get-involved/), from testing, coding, translating, and documentation writing, to just sharing your artwork made with Krita.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Changes
Notable changes in Krita's development builds from Dec. 12, 2024 - Jan. 16, 2025.

### Stable branch (5.2.9-prealpha):
* Filters: Fix compatibility of Per-Channel filter with files from before Krita 5.1. ([bug report](https://bugs.kde.org/497336)) ([Change](https://invent.kde.org/graphics/krita/-/commit/28ba650089), by Dmitry Kazakov)
* General: Improve pattern icon rendering. Fix pattern preview scale. Fix aspect ratio of resource tooltips. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2283), by Freya Lupen)
* File Formats: JPEG XL: Fix unable to set EPF value to -1 (encoder chooses) on export. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2309), by Rasyuqa A H)
* G'MIC Filters: Update to latest version of G'MIC, 3.5.0.1. ([Change](https://invent.kde.org/graphics/krita/-/commit/8b90f60de2))

### Unstable branch (5.3.0-prealpha):
Bug fixes:
* Brush Engines: Fix a regression causing opacity to be multiplied twice in some brush engines (Curve, Clone, Deform, Hairy, Hatching, Sketch, and Spray) since 5.2.0. ([bug report](https://bugs.kde.org/466368)) ([Change](https://invent.kde.org/graphics/krita/-/commit/ea2372b28f), by Dmitry Kazakov)
* Color Management: When Softproofing, use Blackpoint Compensation if enabled. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2279), Wolthera van Hövell)
* Edit Shapes Tool: Fix first click on node being ignored. ([bug report](https://bugs.kde.org/411855)) ([Change](https://invent.kde.org/graphics/krita/-/commit/2b7df579d9), by Dmitry Kazakov)
* Edit Shapes Tool: Change "break path" shortcut in Shape Edit Tool to Ctrl+B to avoid conflict with the default Brush Tool shortcut B.([bug report](https://bugs.kde.org/429503)) ([Change](https://invent.kde.org/graphics/krita/-/commit/228b8fbf9b), by Dmitry Kazakov)


Features:
* General: Fix regressions in Bundle Creator. Fix regressions of resource icons. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2283), by Freya Lupen)
* General: Add setting in General->Miscellaneous to make 'Copy of' on duplicated layer name optional. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2308), by Freya Lupen)

## Nightly Builds
Pre-release versions of Krita are built every day for testing new changes.

Get the latest bugfixes in **Stable** "Krita Plus" (5.2.9-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos-universal) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)

Or test out the latest **Experimental** features in "Krita Next" (5.3.0-prealpha). Feedback and bug reports are appreciated!: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos-universal) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)

