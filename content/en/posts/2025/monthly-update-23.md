---
title: "Krita Monthly Update - Edition 23"
date: "2025-02-12"
---
Welcome to the [@Krita-promo](https://krita-artists.org/groups/krita-promo) team's January 2025 development and community update.

## Development Report
### Krita 5.2.9 Released
A new bugfix release is out! [Check out the Krita 5.2.9 release post](https://krita.org/en/posts/2025/krita-5-2-9-released/) and keep up-to-date with the latest version.

### Qt6 Port Progress
Krita can now be compiled ([MR!2306](https://invent.kde.org/graphics/krita/-/merge_requests/2306)) and run ([Mastodon post](https://fosstodon.org/@halla/113945958080701380)) with Qt6 on Linux, a major milestone on the long road of porting from the outdated Qt5 framework. However, it's still a long way to go to get things working correctly, and it will be some time before any pre-alpha builds are available for the far-off Krita 6.0.

## Community Report
### January 2025 Monthly Art Challenge Results
For the ["Magical Adventure"](https://krita-artists.org/t/monthly-art-challenge-january-2025-magical-adventure/111018) theme, 14 members submitted 20 original artworks.
And the winner is… 
[Magical Adventure by @Mythmaker](https://krita-artists.org/t/jan-2025-challenge-piece-magical-adventure-updated/114492)
<figure>
<a href="images/posts/2025/mu23_magical_adventure-mythmaker.jpeg"> <img class="fit" src="images/posts/2025/mu23_magical_adventure-mythmaker.jpeg" alt="Magical Adventure by @Mythmaker"/> </a>
</figure>

### The February Art Challenge is Open Now
For the February Art Challenge, @Mythmaker has chosen ["Fabulous Flora"](https://krita-artists.org/t/monthly-art-challenge-february-2025-fabulous-flora/114182) as the theme, with the optional challenge of using natural texture. See the full brief for more details, and bring some color into bloom.

## Featured Artwork
### Best of Krita-Artists - December 2024/January 2025
Nine images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-december-2024-january-2025-nomination-submissions-thread/109518), which was open from December 14th to January 11th. When the poll closed on January 14th, these five wonderful works made their way onto the Krita-Artists featured artwork banner:

[Coven Camille | League Of Legends Fan Art by @Dehaf](https://krita-artists.org/t/coven-camille-league-of-legends-fan-art/107210)
<figure>
<a href="images/posts/2025/mu23_coven_camille_league_of_legends_fan_art-dehaf.jpeg"> <img class="fit" src="images/posts/2025/mu23_coven_camille_league_of_legends_fan_art-dehaf.jpeg" alt="Coven Camille | League Of Legends Fan Art by @Dehaf"/> </a>
</figure>

[Still Evening by @MangooSalade](https://krita-artists.org/t/still-evening/112076)
<figure>
<a href="images/posts/2025/mu23_still_evening-mangoosalade.jpeg"> <img class="fit" src="images/posts/2025/mu23_still_evening-mangoosalade.jpeg" alt="Still Evening by @MangooSalade"/> </a>
</figure>

[Themis by @DavB](https://krita-artists.org/t/themis/110874)
<figure>
<a href="images/posts/2025/mu23_themis-davb.jpeg"> <img class="fit" src="images/posts/2025/mu23_themis-davb.jpeg" alt="Themis by @DavB"/> </a>
</figure>

[Flying Pig Squadron by @Yaroslavus_Artem](https://krita-artists.org/t/flying-pig-squadron/112122)
<figure>
<a href="images/posts/2025/mu23_flying_pig_squadron-yaroslavus_artem.jpeg"> <img class="fit" src="images/posts/2025/mu23_flying_pig_squadron-yaroslavus_artem.jpeg" alt="Flying Pig Squadron by @Yaroslavus_Artem"/> </a>
</figure>

[Oniwakamaru and the giant carp by @GioArtworks](https://krita-artists.org/t/oniwakamaru-and-the-giant-carp/110625)
<figure>
<a href="images/posts/2025/mu23_oniwakamaru_and_the_giant_carp-gioartworks.jpeg"> <img class="fit" src="images/posts/2025/mu23_oniwakamaru_and_the_giant_carp-gioartworks.jpeg" alt="Oniwakamaru and the giant carp by @GioArtworks"/> </a>
</figure>

### Best of Krita-Artists - January/February 2025
[Voting is open](https://krita-artists.org/t/best-of-krita-artists-jan-feb-2025-nomination-submissions-thread/112544) until February 15th!

## Ways to Help Krita
Krita is Free and Open Source Software developed by an international team of sponsored developers and volunteer contributors.

Visit [Krita's funding page](https://krita.org/en/donations/) to see how user donations keep development going, and explore a one-time or monthly contribution. Or check out more ways to [Get Involved](https://krita.org/en/get-involved/), from testing, coding, translating, and documentation writing, to just sharing your artwork made with Krita.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Changes
Notable changes in Krita's development builds from Jan. 16 - Feb. 12, 2025.
### Unstable branch (5.3.0-prealpha):
Bug fixes:
* Blending Modes: Rewrite blending modes to properly support float and HDR colorspaces. ([bug report](https://bugs.kde.org/363153)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2294), by Dmitry Kazakov)
* Brush Engines: Fix Filter Brush engine to work with per- and cross-channel filters. ([Change](https://invent.kde.org/graphics/krita/-/commit/79d96d41f0), by Dmitry Kazakov)
* Filters: Screentone: Change default screentone interpolation type to Linear. ([Change](https://invent.kde.org/graphics/krita/-/commit/8374969c46), by Emmet O'Neill)
* Scripting: Fix Node.paint script functions to use the given node instead of active node. ([Change](https://invent.kde.org/graphics/krita/-/commit/8227190c6a), by Freya Lupen)

Features:
* Text: Load font families as resources and display a preview in the font chooser. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2214), by Wolthera van Hövell)
* Filters: Random Noise: Add grayscale noise option and improve performance. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2315), by Maciej Jesionowski)
* Blending Modes: Add a new HSY blending mode, "Tint", which colorizes and slightly lightens. It's suggested to be used with the Fast Color Overlay filter. ([Change 1](https://invent.kde.org/graphics/krita/-/merge_requests/2285), [Change 2](https://invent.kde.org/graphics/krita/-/merge_requests/2318) by Maciej Jesionowski)

## Nightly Builds
Pre-release versions of Krita are built every day for testing new changes.

Get the latest bugfixes in **Stable** "Krita Plus" (5.2.10-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos-universal) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)

Or test out the latest **Experimental** features in "Krita Next" (5.3.0-prealpha). Feedback and bug reports are appreciated!: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos-universal) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)

