---
title: "Krita 5.2 Release Candidate is out!"
date: "2023-09-15"
categories: 
  - "development-builds"
---

The release candidate is here for Krita 5.2, this means that we are confident that all the major bugs brought on by [the changes in Krita 5.2](en/release-notes/krita-5-2-release-notes) have now been fixed, and would like you to give it another round of testing.

Please pay extra attention to the following features of Krita, since they got updated or reworked since Beta2:

- assignment of profiles to displays in multi-monitor setup (Krita should use EDID info to map the displays to profiles everywhere, except on macOS)
- dockers layout should now be properly restored after Krita restart, even after the usage of canvas-only mode
- autokeyframing feature of animated layers got a lot of fixes since Beta2

Here is the full list of bugs (and other minor changes) have been fixed since the [second beta](https://krita.org/en/posts/2023/second-beta-for-krita-5-2-0-released/):

- Fix crash when activating Halftone filter in Filter Brush ([Bug 473242](https://bugs.kde.org/show_bug.cgi?id=473242))
- Fix a crash when activating Color Index filter in the filter brush ([Bug 473242](https://bugs.kde.org/show_bug.cgi?id=473242))
- text: Write xml:space as XML attribute in SVG output
- text: Normalize linebreaks into LF when loading from SVG
- Build patched libraqm with Krita instead of in 3rdparty deps
- \[qtbase\] Correctly parse non BMP char refs in the sax parser
- Actually load the fonts in the QML theme ([Bug 473478](https://bugs.kde.org/show_bug.cgi?id=473478))
- Fix Channels docker to generate thumbnails asynchronously ([Bug 473130](https://bugs.kde.org/show_bug.cgi?id=473130))
- Fix wobbly lines when using line tool ([Bug 473459](https://bugs.kde.org/show_bug.cgi?id=473459))
- text: Make sure white-space overrides xml:space
- text: Reject negative line-height in SVG
- Simplified fix for tag selector and checkboxes problem (CC[Bug 473510](https://bugs.kde.org/show_bug.cgi?id=473510))
- Fix creation of a new image from clipboard ([Bug 473559](https://bugs.kde.org/show_bug.cgi?id=473559))
- Make sure that the horizontal mode of the preset chooser is properly initialized
- Hide preset chooser mode button when the chooser is in horizontal mode ([Bug 473558](https://bugs.kde.org/show_bug.cgi?id=473558))
- Only repaint KisShapeLayerCanvas on setImage when really needed
- text: Do not synthesize bold in several cases like fonts that are already bold or variable fonts.
- AnimAudio: Fixed crash when loading animation file with audio attached, due to incompletely constructed canvas.
- Fix a model warning in KisTimeBasedItemModel ([Bug 473485](https://bugs.kde.org/show_bug.cgi?id=473485))
- Don’t recreate the frames when not necessary ([Bug 472414](https://bugs.kde.org/show_bug.cgi?id=472414))
- Fix cross-colorspace bitBlt with channel flags ([Bug 473479](https://bugs.kde.org/show_bug.cgi?id=473479))
- text: Also consider HHEA metrics for default line height ([Bug 472502](https://bugs.kde.org/show_bug.cgi?id=472502))
- Fix BDF font-size matching ([Bug 472791](https://bugs.kde.org/show_bug.cgi?id=472791))
- Make sure that the node emits nodeChanged() signal on opacity change ([Bug 473724](https://bugs.kde.org/show_bug.cgi?id=473724))
- Fix ‘enabled’ state of the actions in the Default Tool ([Bug 473719](https://bugs.kde.org/show_bug.cgi?id=473719))
- Respect statusbar visibility after Welcome page ([Bug 472800](https://bugs.kde.org/show_bug.cgi?id=472800))
- Fix a warning in outline generation code in shape tools ([Bug 473715](https://bugs.kde.org/show_bug.cgi?id=473715))
- Possibly fix a crash when switching animated documents ([Bug 473760](https://bugs.kde.org/show_bug.cgi?id=473760))
- OpenGL: Request DeprecatedFunctions on Windows to fix Intel driver ([Bug 473782](https://bugs.kde.org/show_bug.cgi?id=473782))
- Allow welcome page banner to shrink
- text: Use line-height when flowing text in shape ([Bug 473527](https://bugs.kde.org/show_bug.cgi?id=473527))
- text: Make first word of text-in-shape flush against the shape
- Fix color values under transparent pixels be lost in Separate Image ([Bug 473948](https://bugs.kde.org/show_bug.cgi?id=473948))
- flake: Fix transformation of text path and shape-inside ([Bug 472571](https://bugs.kde.org/show_bug.cgi?id=472571))
- Make sure that Krita correctly specifies video codec for libopenh264 ([Bug 473207](https://bugs.kde.org/show_bug.cgi?id=473207))
- Don’t allow New/Open File buttons to grow taller ([Bug 473509](https://bugs.kde.org/show_bug.cgi?id=473509))
- raqm: Fix Unicode codepoint conversion from UTF-16
- Android: Bump targetSdkVersion to 33
- Fix multiple issues with auto-keyframing code
- Edit Shapes tool: make moving points _move_ points by a delta instead of snapping them to the cursor
- Initialize tool configGroup before optionWidget ([Bug 473515](https://bugs.kde.org/show_bug.cgi?id=473515))
- Fix updates on autokeyframing with onion skins enabled ([Bug 474138](https://bugs.kde.org/show_bug.cgi?id=474138))
- JPEG-XL: fix crash on importing XYB grayscale that needs transform
- JPEG-XL: also apply patches workaround on lossy export
- Fix artifacts when using assistants in images with high DPI ([Bug 436422](https://bugs.kde.org/show_bug.cgi?id=436422))
- Don’t allow closing hidden document views without confirmation ([Bug 474396](https://bugs.kde.org/show_bug.cgi?id=474396))
- logdocker: Fix infinite tail recursion with multiple windows ([Bug 474431](https://bugs.kde.org/show_bug.cgi?id=474431))

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.0-rc1-setup.exe](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-x64-5.2.0-rc1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.0-rc1.zip](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-x64-5.2.0-rc1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-x64-5.2.0-rc1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.0-rc1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-5.2.0-rc1-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.0-rc1.dmg](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-5.2.0-rc1.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen. RC1 packages for Android are signed as usual.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-x86_64-5.2.0-rc1.apk)
- [32 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-x86-5.2.0-rc1.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-arm64-v8a-5.2.0-rc1.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-armeabi-v7a-5.2.0-rc1.apk)

### Source code

- [krita-5.2.0-rc1.tar.gz](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-5.2.0-rc1.tar.gz)
- [krita-5.2.0-rc1.tar.xz](https://download.kde.org/unstable/krita/5.2.0-rc1/krita-5.2.0-rc1.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/unstable/krita/5.2.0-rc1/](https://download.kde.org/unstable/krita/5.2.0-rc1) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. This particular release is signed with a non-standard key, you can retrieve is [here](https://files.kde.org/krita/4DA79EDA231C852B) or download from the public server:

gpg --recv-keys E9FB29E74ADEACC5E3035B8AB69EB4CF7468332F

The signatures are [here](https://download.kde.org/unstable/krita/5.2.0-rc1/) (filenames ending in .sig).
