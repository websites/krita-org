---
title: "Krita Monthly Update – Edition 17"
date: "2024-07-21"
---
Welcome to the latest development and community news curated for you by the @Krita-promo team.

## Development report

### 5.2.3 Released June 26, 2024

This is mainly a bugfix release following weeks of beta testing. Thank you to everyone who tested and reported feedback and a big thank you to everyone who contributed code. Here are just a few highlights:

* Fix copy-pasting selection of File Layer ([Bug 459849](https://bugs.kde.org/show_bug.cgi?id=459849))
* Reworked default FFmpeg profiles ([Bug 455006 ](https://bugs.kde.org/show_bug.cgi?id=455006), [450790](https://bugs.kde.org/show_bug.cgi?id=450790), [429326](https://bugs.kde.org/show_bug.cgi?id=429326), [485515](https://bugs.kde.org/show_bug.cgi?id=485515), [485514](https://bugs.kde.org/show_bug.cgi?id=485514)). Thanks @Ralek!
* Tablet Tester: Fix extreme lag with S Pen on Android – Thanks Joshua Goins!

Read the [full release notes ](https://krita.org/en/posts/2024/krita-5-2-3-released/) on [Krita.org](http://Krita.org).

### New Features and fixes

* A new python painting API by @freyalupen based on previous unfinished work was merged. Now you can utilize python to programmatically draw objects and other stuff on the canvas.
* @YRH fixed an issue with audio and frame rate.
* Related to the python API, some parts of @Grum999’s API work have been merged ([2160](https://invent.kde.org/graphics/krita/-/merge_requests/2160) and [2167](https://invent.kde.org/graphics/krita/-/merge_requests/2167)). Here are a few highlights:
  * Document: Grids & Guides implementation review (dedicated classes available) + miscellaneous minor improvements
  * Palette: minor improvements (try to get something more consistent in API)
  * Manage class cast properly for Python class bindings (always get expected Python class instead of QObject)
  * Updated API documentation for some classes (get more detailed documentation about class & methods)
* @Deif_Lou has implemented a new feature which will help those who paint UV maps. It is called propagate colours. As the name suggests it propagates colours into nearby transparent areas which can be useful in the creation of UV maps.

![Propogate Colors examples](images/posts/2024/mu17_propagate_colors_examples-tomjk-deif_lou.png)

<figcaption>From left to right: the original image; the expanded image with expanded alpha channel; the expanded image preserving the alpha channel (note that it looks like the original one, although the color components were propagated into the transparent areas); the third image after applying a levels filter that makes the transparent areas semi-opaque to clearly see how the colors were truly propagated. Images by @tomjk and @Deif_Lou.</figcaption>

### Other Development Highlights

* @Tiar has put out a [call for artwork donations ](https://krita-artists.org/t/call-for-donation-of-artworks-for-the-fast-line-art-project/96401) for the Fast Line Art tool currently in development. This is an experimental feature sponsored by Intel. The tool’s purpose is to speed up the creation of line art from the artist’s rough sketch using neural networks. The donated work will be used to train the tool.
* Google Summer of Code (GSOC) [Pixel Perfect Line Setting for Pixel Art Brushes](https://krita-artists.org/t/pixel-perfect-line-setting-for-pixel-art-brushes/42629/) is moving forward. @Ken_Lo published a [Week 5 Recap ](https://kenlo.hashnode.dev/week-5-recap) outlining a proposed vector approach.

### Krita’s 25-year Anniversary Celebration Continues

@RamonM released a second video commemorating this incredible milestone. He is joined by Lucas Tvrdy, an early Krita developer, and @Animtim, creator of Krita’s first book and training DVD. There is even a brief musical message from @Deevad, a professional artist and long-time Krita supporter. Watch the video to see who else pops in with a greeting.

https://www.youtube.com/watch?v=DifqGFFAnsE 

### June 2024 Monthly Art Challenge Results

Participation in “Magnificent Dragon” was fabulous with 32 members submitting 36 original artworks. Thank you, @Elixiah, for giving us a great challenge. The resulting tie for first place between @Elixiah and @Brinck was decided on by the moderators and @Brinck was named the [winner ](https://krita-artists.org/t/magnificent-dragon-june-2024-challenge-winner/95212).

![Magnificent Dragon by Brinck](images/posts/2024/mu17_magnificent_dragon-brinck.jpeg)


### The July Art Challenge is Open Now

For the July Art Challenge, @Brinck has chosen this topic: [Still life from another world ](https://krita-artists.org/t/monthly-art-challenge-july-2024-still-life-from-another-world/95264). There is also an additional, optional challenge to produce the work in gauntlet mode. Read the brief for a full description.

## Featured artwork

### Best of Krita-Artists May/June

Ten images were nominated and then voted on in the May/June [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-may-june-2024-nomination-submissions-thread/91535). When the poll closed on June 14th, these five received the most votes and were added to the Krita-Artists featured artwork banner and were also shared on social media sites (with permission, of course).

**[Sunset on the quay ](https://krita-artists.org/t/sunset-on-the-quay/93450) by @Sad_Tea**

![Sunset on the quay by Sad_Tea](images/posts/2024/mu17_sunset_on_the_quay-sad_tea.jpeg)

[Sculpture study 1](https://krita-artists.org/t/sculpture-study-1/89846) by @024XS

![Sculpture study 1 by 024XS](images/posts/2024/mu17_sculpture_study_1-024xs.jpeg)

[Cosmos LTD](https://krita-artists.org/t/cosmos-ltd/93142) by @Yaroslavus_Artem

![Cosmos LTD by Yaroslavus_Artem](images/posts/2024/mu17_cosmos_ltd-yaroslavus_artem.jpeg)

[Sparkle](https://krita-artists.org/t/sparkle/92942) by @My.last-charade

![Sparkle by My.last-charade](images/posts/2024/mu17_sparkle-my_last-charade.jpeg)

[Rippling Ribbons](https://krita-artists.org/t/rippling-ribbons/91173) by SylviaRitter

![Rippling Ribbons by SylviaRitter](images/posts/2024/mu17_rippling_ribbons-sylviaritter.jpeg)

## Noteworthy plugin

[Theme Creator Extension ](https://krita-artists.org/t/plugin-theme-creator-extension/62953) by @freyalupen.

This plugin allows users to create a basic version of a color theme file, with partial previewing while choosing colors (Note: a restart is required to apply all effects).

![Theme Creator Extension screenshot](images/posts/2024/mu17_theme_creator_extension-freyalupen.png)

## Tutorial of the month

[The ultimate guide to printing digital art accurately: Eliminate Color Shifts! ](https://youtu.be/4EW32Q6UzxY) by @CelticCoco.

How to:

* Achieve color-accurate prints every time
* Calibrate your digital workflow
* Ensure Krita and your devices all speak the same “color language”

![Eliminate Color Shifts](images/posts/2024/mu17_eliminate_color_shifts-celticcoco.png)

## Ways to help Krita

Krita is a Free and Open Source application, mostly developed by an international team of enthusiastic volunteers. Donations from Krita users to support maintenance and development is appreciated.

Visit [Krita’s funding page ](https://krita.org/en/donations/) to see how donations are used and explore a one-time or monthly contribution.

## Notable changes in code

This section has been compiled by @freyalupen.

## Jun 11 - Jul 7, 2024

Stable branch (5.2.3):
Bugfixes:

* [Windows: Canvas] Fix canvas fading out when zooming out on 16-bit images when using ANGLE canvas renderer on Windows, a regression present in 5.2.3-beta1. ([BUG:488126](https://bugs.kde.org/488126)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/de655f2a6154a507bfe7ea3289a7d241e293f276))
* [Touch Docker] Implement a QWidget version of the touch docker with less features that isn’t completely broken, until a proper QML version can be reimplemented. ([BUG:476690 ](https://bugs.kde.org/476690)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/d50ba363484dd061a0ebf4dc1c73a92000d369b8))
* [Tools: Color Sampler] Fix color sampler to work in wrap around mode. ([BUG:478190](https://bugs.kde.org/478190)) ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2173))
* [Layer Stack] Fix copy-pasting a selection of a File Layer. ([BUG:459849 ](https://bugs.kde.org/459849)) ([commit, Freya Lupen ](https://invent.kde.org/graphics/krita/-/commit/09ff812f8316eea0a6bf55d49ec093c6a3ea41d2))
* [Plugins: Batch Exporter] Fix Batch Exporter plugin ignoring trimming in some cases. ([BUG:488343](https://bugs.kde.org/488343)) ([merge request, Nathan Lovato](https://invent.kde.org/graphics/krita/-/merge_requests/2168))
* [Tablet Tester] Fix extreme lag in the Tablet Tester with S Pen on Android. ([merge request, Joshua Goins ](https://invent.kde.org/graphics/krita/-/merge_requests/2172))
* [Animation] Fix animation playback freezes when pausing past the end of audio. ([BUG:487371](https://bugs.kde.org/487371)) ([BUG:478185](https://bugs.kde.org/478185))) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/ba9b00213e79c0eeabd3d48a310bd035a69ac078))
* [Recorder Docker] Prevent some tools from being interrupted by the recorder, by disabling the recorder when they are active. The following tools’ actions won’t be recorded: Transform, Polyline, Polygon, Enclose and Fill, Freehand Path, Crop, Freehand Selection, Polygonal Selection, Bezier Curve Selection, Magnetic Curve Selection. The following tools completely disable recording while they are active: Move, Line, Rectangle, Ellipse, Rectangular Selection, Elliptical Selection. ([BUG:477715](https://bugs.kde.org/477715), [BUG:484783](https://bugs.kde.org/484783)) ([merge request, Firstname Aqaao](https://invent.kde.org/graphics/krita/-/merge_requests/2164))

Stable branch (5.2.3+):
Bugfixes:

* [Animation] Fix audio duration not being updated after animation framerate is updated. ([merge request, Maciej Jesionowski ](https://invent.kde.org/graphics/krita/-/merge_requests/2183))

Stable branch (5.2.3) backports from Unstable:
Bugfixes:

* [File Formats] WEBP & JPEG-XL: Prevent crash/error when attempting to export a non-animated image as animated. ([BUG:476761](https://bugs.kde.org/476761)) ([merge request, Rasyuqa A H](https://invent.kde.org/graphics/krita/-/merge_requests/2025))

---

Unstable branch (5.3.0-prealpha):
Features:

* [Filters] Add Colors->“Propagate Colors” filter, which expands colors into transparent areas, while optionally keeping the alpha value untouched. ([merge request, Deif Lou ](https://invent.kde.org/graphics/krita/-/merge_requests/2162))
* [File formats] Implement CSS Palette support. ([WISHBUG:367694](https://bugs.kde.org/367694)) ([merge request, Thomas K/BIDI](https://invent.kde.org/graphics/krita/-/merge_requests/2119))
* [Layer Stack] Enable setting colorspace of Group Layers. ([merge request, killy |0veufOrever](https://invent.kde.org/graphics/krita/-/merge_requests/1978))
* [Canvas Input] Add Zoom In To Cursor and Zoom Out From Cursor canvas input shortcuts, as an alternative to regular Zoom In/Out which use the center of the canvas. ([merge request, Mr. xk1000](https://invent.kde.org/graphics/krita/-/merge_requests/2152))
* [Scripting] Add functions to modify the state of Grids and Guides to the Document API. ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2167))
* [Scripting] Add functions to change the autosave state to the Document API. ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2160))
* [Templates] Add two new Japanese comic format templates. ([merge request, Jun Shiozawa ](https://invent.kde.org/graphics/krita/-/merge_requests/2186))

Bugfixes:

* [Android: Usability] Fix applying size adjustments to the Android system font, improving readability of docker text in small spaces by giving it the intended 90% size as on other platforms or with a custom UI font. ([merge request, Freya Lupen ](https://invent.kde.org/graphics/krita/-/merge_requests/2185))
* [File formats: EXR] Partially fix loading XYZ channels on EXR import. ([CCBUG:487103](https://bugs.kde.org/487103)) ([merge request, Rasyuqa A H](https://invent.kde.org/graphics/krita/-/merge_requests/2182))

---

These changes are made available for testing in the following Nightly builds:

* Stable “Krita Plus” (5.2.3+): [Linux ](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows ](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) [unsigned currently] - Android ([arm64-v8a ](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64))
* Unstable “Krita Next” (5.3.0-prealpha): [Linux ](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows ](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) [unsigned currently] - Android ([arm64-v8a ](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64))

{{< support-krita-callout >}}
