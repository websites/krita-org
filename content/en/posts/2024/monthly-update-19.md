---
title: "Krita Monthly Update - Edition 19"
date: "2024-10-07"
---
Welcome to the [@Krita-promo](https://krita-artists.org/groups/krita-promo) team's September 2024 development and community update.

## Development Report
### Krita 5.2.6 Released 
Krita 5.2.5 was released September 25th. However, due to a serious bug where Group Layers in Pass Through mode caused Krita to get stuck endlessly updating, a 5.2.6 hotfix release was made shortly afterward on October 1st.

Check out the [5.2.5 release post](https://krita.org/en/posts/2024/krita-5-2-5-released/) for details on the three months' worth of bugfixes, but make sure to [update to 5.2.6](https://krita.org/en/posts/2024/krita-5-2-6-released/)!
## Community Report
### September 2024 Monthly Art Challenge Results
For the [Traditional Refreshments and Snacks](https://krita-artists.org/t/monthly-art-challenge-september-2024-topic-traditional-refreshments-and-snacks/100673/) theme, 12 members submitted 13 original artworks.
And the winner is… [Sweet Lemonade](https://krita-artists.org/t/sweet-lemonade/103064) by @Dott!
<figure>
<a href="images/posts/2024/mu19_sweet_lemonade-dott.jpeg"> <img class="fit" src="images/posts/2024/mu19_sweet_lemonade-dott.jpeg" alt="Sweet Lemonade by @Dott"/> </a>
</figure>

### The October Art Challenge is Open Now
For the October Art Challenge, @Dott has chosen ["Buried, Stuck, or otherwise Swallowed"](https://krita-artists.org/t/monthly-art-challenge-october-2024-topic-buried-stuck-or-otherwise-swallowed/103132) as the theme, with the optional challenge of incorporating a spiral or two. See the full brief for more details, and don't hesitate to uncover some inspiration!

## Featured Artwork
### Best of Krita-Artists - August/September 2024
10 images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-august-september-2024-nomination-submissions-thread/99354/), which was open from August 15th to September 11th. When the poll closed on September 14th, these five wonderful works were voted into the Krita-Artists featured artwork banner:

[Study practice](https://krita-artists.org/t/study-practice/100426) by @Gertsa_Ivan
<figure>
<a href="images/posts/2024/mu19_study_practice-gertsa_ivan.jpeg"> <img class="fit" src="images/posts/2024/mu19_study_practice-gertsa_ivan.jpeg" alt="Study practice by @Gertsa_Ivan"/> </a>
</figure>

[Kiki recruiting for the Monthly Challenge](https://krita-artists.org/t/kiki-recruiting-for-the-monthly-challenge/101564) by @Corentin
<figure>
<a href="images/posts/2024/mu19_kiki_recruiting_for_the_monthly_challenge-corentin.jpeg"> <img class="fit" src="images/posts/2024/mu19_kiki_recruiting_for_the_monthly_challenge-corentin.jpeg" alt="Kiki recruiting for the Monthly Challenge by @Corentin"/> </a>
</figure>

["Friend from Nowhere"](https://krita-artists.org/t/friend-from-nowhere/99291) by @Elixiah
<figure>
<a href="images/posts/2024/mu19_friend_from_nowhere-elixiah.jpeg"> <img class="fit" src="images/posts/2024/mu19_friend_from_nowhere-elixiah.jpeg" alt="Friend from Nowhere by @Elixiah"/> </a>
</figure>

[XullFA](https://krita-artists.org/t/xullfa/100817) by @tonnybranzza
<figure>
<a href="images/posts/2024/mu19_xullfa-tonnybranzza.jpeg"> <img class="fit" src="images/posts/2024/mu19_xullfa-tonnybranzza.jpeg" alt="XullFA by @tonnybranzza"/> </a>
</figure>

[Portrait of a woman in profile](https://krita-artists.org/t/portrait-of-a-woman-in-profile/96197/1) by @Celes
<figure>
<a href="images/posts/2024/mu19_portrait_of_a_woman_in_profile-celes.jpeg"> <img class="fit" src="images/posts/2024/mu19_portrait_of_a_woman_in_profile-celes.jpeg" alt="Portrait of a woman in profile by @Celes"/> </a>
</figure>

### Best of Krita-Artists - September/October 2024
Nominations will be accepted until October 11th. Take a look through the current submissions. If you don’t see your favorite artwork there, why not make a nomination?
## Ways to Help Krita
Krita is Free and Open Source Software developed by an international team of sponsored developers and volunteer contributors.

Visit [Krita's funding page](https://krita.org/en/donations/) to see how user donations keep development going, and explore a one-time or monthly contribution. Or check out more ways to [Get Involved](https://krita.org/en/get-involved/), from testing, coding, translating, and documentation writing, to just sharing your artwork made with Krita.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Changes
Notable changes in Krita's development builds from Sept. 6 - Oct. 7, 2024.
### Stable branch (5.2.5):
* Animation: Fix animations getting stuck when using clone layers with transform masks. [(bug report)](https://bugs.kde.org/484353) ([Change](https://invent.kde.org/graphics/krita/-/commit/0dd3c7bf03), by Dmitry Kazakov)
* Animation: Fix onion skins' rendering on layers with transform masks. [(bug report)](https://bugs.kde.org/457136) ([Change](https://invent.kde.org/graphics/krita/-/commit/ce7a8c5fca), by Dmitry Kazakov)
* Animation: Make Render Animation dialog remember previous video container format. [(bug report)](https://bugs.kde.org/485666) ([Change](https://invent.kde.org/graphics/krita/-/commit/94072709d3), by Emmet O'Neill)
* Animation: Allow video animation import process to be canceled. [(bug report)](https://bugs.kde.org/436964) ([Change](https://invent.kde.org/graphics/krita/-/commit/4c62b784ba), by Emmet O'Neill)
* Compositions Docker: Allow animated composition export to be canceled. [(bug report)](https://bugs.kde.org/437020) ([Change](https://invent.kde.org/graphics/krita/-/commit/ad76c60927), by Emmet O'Neill)
* Layer Stack: Fix conversion of Group Layer to Animated Layer. [(bug report)](https://bugs.kde.org/476583) ([Change 1](https://invent.kde.org/graphics/krita/-/commit/fa805917e1) and [change 2](https://invent.kde.org/graphics/krita/-/commit/424f716ea7), by Emmet O'Neill)
* Undo Stack: Fix quickly undoing and then redoing some actions like Remove Layer to work. [(bug report)](https://bugs.kde.org/491186) ([Change](https://invent.kde.org/graphics/krita/-/commit/36d76c72d9), by Dmitry Kazakov)
* Layer Stack: Keep Clone Layers linked when flattening their source group. [(bug report)](https://bugs.kde.org/476514) ([Change](https://invent.kde.org/graphics/krita/-/commit/979965d84c), by Dmitry Kazakov)
* Layer Stack: Fix updating Pass Through groups when using the Move or Transform Tool on them. [(bug report)](https://bugs.kde.org/457957) ([Change](https://invent.kde.org/graphics/krita/-/commit/c2c12c46bf), by Dmitry Kazakov)
* Pasting: Deselect selection when pasting from the clipboard, to avoid leaving anti-aliased selection edges out of Transform or other operations that might happen afterward. [(bug report)](https://bugs.kde.org/459162) ([Change](https://invent.kde.org/graphics/krita/-/commit/d33f52e4c8), Dmitry Kazakov)
* Snapshot Docker: Fix wrong layer being selected when activating a snapshot. [(bug report)](https://bugs.kde.org/492114) ([Change](https://invent.kde.org/graphics/krita/-/commit/60c5dcaf37), by Dmitry Kazakov)
* Specific Color Selector Docker: Update Specific Color Selector labels in HSX mode, fix RGB sliders changing length, fix holding down spinbox arrows, and set float slider step to 0.01.  ([bug 475551](https://bugs.kde.org/475551), [bug 453649](https://bugs.kde.org/453649), CC [bug 453366](https://bugs.kde.org/453366)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2231), by Freya Lupen)
* Assistant Tool: Fix undoing changes to Perspective Assistants causing Perspective Distance brush value to be incorrect. [(bug report)](https://bugs.kde.org/493185) ([Change](https://invent.kde.org/graphics/krita/-/commit/73025b43bf), by Dmitry Kazakov)
* File Formats: JPEG-XL: Update JXL to 0.11 which includes fixes for speed and lossless encoding, and implement streaming encode which allows cancelling animated and layered export. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2234), by Rasyuqa A H)

### Stable branch (5.2.6):
* Layer Stack: Fix updating Pass Through groups getting stuck at 0% progress forever, a regression in 5.2.5. ([bug 493774](https://bugs.kde.org/493774), [bug 493830](https://bugs.kde.org/493830), [bug 493837](https://bugs.kde.org/493837)) ([Change](https://invent.kde.org/graphics/krita/-/commit/dcaf7fe2ab), by Dmitry Kazakov)
* Python Plugins: Photobash Images Docker: Fix script error when adding images to canvas from the Photobash Images Docker. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2244), by Pedro Reis)

### Stable branch (5.2.7-prealpha):
* Layer Stack: Fix a crash when trying to merge down a layer created by the Text Tool or Select->Convert to Shape after a reference image addition. [(bug report)](https://bugs.kde.org/494122) ([Change](https://invent.kde.org/graphics/krita/-/commit/a6428902cf), by Dmitry Kazakov)
* Layer Stack: Fix issue with position of layers copy-pasted from another document. [(bug report)](https://bugs.kde.org/490998) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2213), by Ralek Kolemios and Dmitry Kazakov)
* Tools: Let brush shortcuts such as to change size affect the Bezier Curve and Freehand Path tools.  [(bug report)](https://bugs.kde.org/459726) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2232), by Doreene Kang)
* General: Add A1 and A2 predefined image sizes. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2245), by Guillaume Marrec)


### Unstable branch (5.3.0-prealpha):
Bug fixes:
* macOS: Canvas Input: Show macOS modifier key symbols in Canvas Input Settings, as Keyboard Shortcuts already does, instead of the non-Mac names. [(bug report)](https://bugs.kde.org/388394)  ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2229), by Freya Lupen)

Features:
* Animation Timeline Docker: Adjust the frame view's zoom level to fit the frame range when it changes. ([wishbug report](https://bugs.kde.org/455818)) ([Change](https://invent.kde.org/graphics/krita/-/commit/2d13e91250), by Emmet O'Neill)

## Nightly Builds
Pre-release versions of Krita are built every day for testing new changes.

Get the latest bugfixes in **Stable** "Krita Plus" (5.2.7-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)

Or test out the latest **Experimental** features in "Krita Next" (5.3.0-prealpha). Feedback and bug reports are appreciated!: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)


## Have feedback?
Join the [discussion of this post](https://krita-artists.org/t/krita-monthly-update-edition-19/103720) on the [Krita-Artists forum](https://krita-artists.org/)!
