---
title: "Krita 5.2.6 Released!"
date: "2024-10-01"
categories: 
  - "news"
  - "officialrelease"
---

Krita 5.2.6 fixes a critical error with pass-through group layers (https://bugs.kde.org/show_bug.cgi?id=493774).

## Download

### Windows

If you're using the *portable zip files*, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note: We are no longer making 32-bit Windows builds.

- 64 bits Windows Installer: [krita-5.2.6-setup.exe](https://download.kde.org/stable/krita/5.2.6/krita-x64-5.2.6-setup.exe)
- Portable 64 bits Windows: [krita-5.2.6.zip](https://download.kde.org/stable/krita/5.2.6/krita-x64-5.2.6.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.6/krita-x64-5.2.6-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.6-x86\_64.appimage](https://download.kde.org/stable/krita/5.2.6/krita-5.2.6-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: right-click on the link to download.)

### MacOS

Note: We're not supporting MacOS 10.13 anymore, 10.14 is the minimum supported version.

- MacOS disk image: [krita-5.2.6-release.dmg](https://download.kde.org/stable/krita/5.2.6/krita-5.2.6-release.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.6/krita-x86_64-5.2.6-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.6/krita-arm64-v8a-5.2.6-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.6/krita-armeabi-v7a-5.2.6-release-signed.apk)

### Source code

- [krita-5.2.6.tar.gz](https://download.kde.org/stable/krita/5.2.6/krita-5.2.6.tar.gz)
- [krita-5.2.6.tar.xz](https://download.kde.org/stable/krita/5.2.6/krita-5.2.6.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.6/](https://download.kde.org/stable/krita/5.2.6) and click on "Details" to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/dmitry_kazakov.gpg). The signatures are [here](https://download.kde.org/stable/krita/5.2.6/) (filenames ending in .sig).
