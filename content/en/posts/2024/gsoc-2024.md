---
title: "Picky Pixels: Krita's GSoC 2024!"
date: "2024-09-10"
categories:
  - "news"
  - "gsoc"
  - "sponsorships"
---

Well, somehow another summer* has come to a close and, with it, this year's Google Summer of Code has all but wrapped up!

# A little GSoC speedrun

If you aren't familiar with [Google Summer of Code](https://summerofcode.withgoogle.com/) ("GSoC", for the rest of this post to save my weary fingers), it's a yearly program that Google runs in which they pay people to successfully contribute features and improvements to free and open source software ("FOSS") projects like ours. It's hopefully a win-win for all parties involved: it's a decent summer gig and learning experience for the contributor, it incentivizes new people to dip their toes into the world of FOSS contribution, and it theoretically strengthens the ecosystem of FOSS software that companies like Google rely heavily on.

Like many other FOSS projects, Krita has been a participant in GSoC for a number of years, and successful projects have brought about a smattering of nice improvements, like the improved Resource Manager, the all new Storyboard Docker, the Recorder Docker, and so on.

Best of all, in recent years Google has opened the door to everybody (not just higher education students anymore!), so that people from every walk of life and background can participate. Of course, you probably need to have at least a foundational knowledge of programming and solid communication skills to succeed, but I really appreciate that the program has become much more open to equitable. (Good move Google!) 

So, if that sounds at all interesting and you think you have what it takes, GSoC can be a good way to get paid to contribute to your favorite FOSS projects.

# Pixel almost-perfect

This year's GSoC was a bit of a quiet one, as we only had one project: "Pixel Perfect Lines" by Ken Lo, with the simple goal of creating a better workflow experience for pixel/sprite artists by allowing for "perfect" single-pixel lines.

You might be asking yourself, what makes a "perfect" single-pixel line? 

At its core art is about expression, so there really is no such this as objectively good or bad, right or wrong, and so on. Pixel art is no different in that regard, but just like any medium there are certain conventions and traditions that people just like to see. For spriters, one of those conventions is the appeal of clean and legible single-pixel lines, like you might use as the outline of a character or other object. Generally, as I understand it, a conventionally good quality single-pixel line for to pixel artists is one that has minimal right-angle changes of direction (L-shapes, T-shapes, and so on). Avoiding right-angles can give you beautiful and graceful curves--something that you can easily take for granted until you're working at a super low resolution.

Which brings us back to Ken's goal with GSoC 2024: adding a new feature for Krita which allows pixel artists to draw smooth, flowing, single-pixel lines with the stroke of a pen! And I'm happy to share that he pretty much succeeded in that goal, adding a new "Pixel" smoothing option to the Brush Tool which should significantly help the pixel art workflow in Krita. (Coming soon to the Tool Options Docker!)

See for yourself:

![A demo showing lines drawn without and with the new Pixel smoothing mode on. The RED line has smoothing set to None, while the GREEN line has smoothing set to Pixel. The blue pixels are there to show the kinds of pixel artifacts that we're trying to get rid of. NOTE: The RED and GREEN lines were drawn separately by hand, so they aren't going to be perfectly equivalent.](/images/posts/2024/pixel-filtering.png)

As for whether the lines produced are "pixel perfect", well... not yet, but they're damn pretty close! Ken and I really hoped to achieve truly "pixel perfect" pixel art lines to Krita during this GSoC, and Ken did quite a bit of good research into how he might go about achieving that result. Ultimately, however, we came to the conclusion that true pixel perfect would probably require bigger changes to Krita's brush logic than the remaining GSoC time would probably allow. That's alright, and I think a good lesson, too--sometimes the plan you start with hits a dead end and just have to adapt!

In other words, you're still likely to run into an occasional stray pixel that you may want to erase. But what I know for sure is that Ken's contribution to Krita via GSoC 2024 is a much-improved single-pixel line quality that I'm sure will be a big workflow improvement for all of the spriters, pixel artists and game developers who use Krita as one of their tools. 

Of course, we don't plan on giving up until we reach the pinnacle of pixel perfection, so if anyone out there has ideas for how to bring this feature to the next level please drop us a line!

# Ok, cool! But... how?

I almost forgot to tell you how to use it... But the good news is that it's pretty simple.

To turn on the new pixel line stabilizer, select the Brush Tool or similar, open up the Tool Options Docker, and set the "Brush Smoothing" dropdown to "Pixel". You probably (hopefully) won't notice any change to performance or line quality when drawing with a regular brush on a regular canvas, but with the 1px pixel art brush preset active you'll find that drawing flowing strokes produces nicer pixel art lines. 

![Tool Options Docker with "Brush Smoothing" set to "Pixel"](/images/posts/2024/pixel-filtering2.png)


# And that's a wrap!

That'll be it for GSoC 2024!

Thanks to Google for partnering with projects like Krita once again for another successful GSoC, and of course a major thanks is in order to this year's Krita GSoC contributor, Ken!

Finally, whether it's through a program like GSoC, our [Development Fund](https://fund.krita.org/), or the [absolutely massive number of volunteer contributions that we constantly receive from members of the community](https://invent.kde.org/graphics/krita/-/merge_requests), projects like Krita rely on the generosity and support of people like you. So, as always, a GIANT thank you to all of you who have supported this project in any way so far this year! :)

Emmet



*Here in the Northern Hemisphere, but I see you Southern Hemisphere people!
