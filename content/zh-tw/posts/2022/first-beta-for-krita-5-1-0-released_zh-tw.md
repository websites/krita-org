---
title: "發佈 Krita 5.1.0 首個測試版本 (Beta 1)"
date: "2022-06-23"
categories: 
  - "news_zh-tw"
  - "development-builds_zh-tw"
---

今天我們發佈了 Krita 5.1.0 的首個測試版本。Krita 5.1.0 帶來了許多新功能啊！想知道完整的新功能清單的話，可以看看還未寫完的[英文版發佈通告](https://krita.org/en/krita-5-1-release-notes/)喔！

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是自由、免費及開源的專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或[購買教學影片](https://krita.org/en/shop/)支持我們吧！得到您們的熱心支持，我們才能夠讓核心開發者全職為 Krita 工作。

## 重點新功能

- 更多操作可支援同時處理多於一個已選取圖層。
- 我們改善了對 WebP 檔案、帶有 Photoshop 圖層的 TIFF 檔案，以及 Photoshop 檔案的支援。同時，我們亦新增了支援 JPEG XL 檔案格式。（但請留意下方的已知問題。）
- Krita 現在使用了 XSIMD 程式庫來取代 Vc 作 SIMD（單指令多資料流）加速，以進一步提升筆刷效能。這對於在 Android 裝置上運行的效能影響尤其大，因為這是首個能支援 ARM 架構的 SIMD 加速指令集的 Krita 版本。
- 填充工具已擴展至支援「多重填充」（連續模式），另外也新增了一個「閉合區域填充工具」。
- 我們為 Windows 版本更新了 ANGLE 程式庫，以改善圖形加速的兼容性和效能。
- 您現在可在「畫布輸入設定」中設定觸控手勢操作，如使用兩指點選作復原。

當然，還有大量各項修正、效能提升、介面改進，連帶動畫功能也有些改善。（不過動畫音效系統的大修未能趕及在這個版本推出，要留待 5.2 版本了。）

## 已知問題

- 這個版本可以自訂觸控手勢輸入，不過如您在舊版有使用到觸控輸入的話，您需要到「畫布輸入設定」中重新加入並設定觸控手勢，方可使觸控輸入生效。
- 雖然這個版本已加入了 JPEG XL 動畫的支援，但是在匯入及匯出 JPEG XL 動畫時已知道會產生不正確的輸出。我們正在修正這個問題。

[![5.1.0-beta1 介面截圖](images/5.1.0-beta1-1024x562.png)](https://krita.org/wp-content/uploads/2022/06/5.1.0-beta1.png)

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.0-beta1-setup.exe](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.0-beta1.zip](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-beta1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.0-beta1.dmg](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86_64-5.1.0-beta1-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86-5.1.0-beta1-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-arm64-v8a-5.1.0-beta1-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-armeabi-v7a-5.1.0-beta1-release-signed.apk)

### 原始碼

- [krita-5.1.0-beta1.tar.gz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.gz)
- [krita-5.1.0-beta1.tar.xz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.xz)
