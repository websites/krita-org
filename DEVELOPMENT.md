# Changing Templates/Layouts

If you go in the themes > krita-org-theme > layouts, you will notice there are a lot of template files there. This is a template that a page can use. If a page(MD file) is using a template, it will have a "type" variable set at the top. This maps directly to the folder in this layout area.

Template variable errors can be very cryptic to read when doing things like looping over content. Do small changes at a time to make it easier to know what went wrong.

Note: If you are running the project and make changes to the template, you might have to stop the server from running (I use Ctrl+C), then restart it again with 'hugo server' to see the changes.

# Adding tabs to groups of pages

Some website sections like the About area has multiple pages inside (History, About, Press, etc ). There are two parts to making this sub-navigation work
1. Adding a "group" variable to the top of each page that needs to be set to the section.
2. Having a template (type variable) that that shows and filters that data. Templates are in the theme > krita-org-theme > layouts folder


## Marking a post or page as a draft so it isn't published/shown
On the top most section of your markdown file (called front matter in Hugo), add a variable with "draft: true"

## Adding images
Images in general are stored in the themes > krita-org-theme > static > images folder. There are two folders that are in here that are only for organizational purposes

1. pages - content that is not news specific
1. posts - news

To avoid naming conflicts, post images are currently broken apart into years. If you are creating a news story in 2022, you would put the images in the posts > 2022 folder.

# Adding videos locally or to the CDN

If the local video files are relatively small 1-2MB, you can put the videos in the videos folder in the themes > krita-org-theme > static > videos folder. If there is a large video, it is best to put it on the KDE CDN so this GIT repository won't explode in size.

Videos are stored at this location on the KDE CDN:
https://cdn.kde.org/krita/marketing/videos/

This server needs special permission to access. It is considered the "Deino" server when requesting access. See example here for a local file:

{{< video-player src="videos/demo.mp4" type="video/mp4" >}}

# Adding YouTube videos and shortcodes
There are shortcuts (called shortcodes) that allow you to easily add more complex things to your markdown files. Here are few simple examples: 

{{< youtube spy-0EKBcvQ >}}

{{< tweet user="SanDiegoZoo" id="1453110110599868418" >}}

{{< vimeo 146022717 >}}


## Adding language specific images
While doing a translation, you might have an image that has text on it that needs to be translated. We generally try to avoid this, but it happens sometimes. After getting/creating a new localized image, put it directly in the posts area where you are creating your MD file for your language. You can create an 'images' folder in the posts folder and put it in there to be more organized. You can locally reference the image in a language by doing something like 'images/my-custom-image.png'. 


# Adding functionality outside of mark down files
Markdown is create, but it has limitations on what it can do and display. To get around this, you can create "shortcodes" in Hugo. This is a template that you create in HTML that you can access in an easy way from markdown. To see the existing custom shortodes, see the themes > krita-org-theme > layouts > shortcodes directory.


# Editing Translations
When editing translations, it will be done in two areas
- i18n folder in the themes 
- the content > language folder

# Adding a new language
You shouldn't have to do anything to update languages. The KDE Hugo module that this project uses manages all of that. The KDE module creates the language drop-down, and there is a pipeline to automatically bring in new translations once a day. If the language is brand new, that will be managed by the KDE i18n team.

